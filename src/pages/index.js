import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';
import Music from '../components/lastfm';
import {Link, Title} from '../components/elements';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Title>{String.fromCodePoint(0x1f62e)}</Title>
    <p>
      yes, this is nagee.{' '}
      <Link href="https://twitter.com/isthisnagee">twitter</Link>.
      <Link href="https://github.com/isthisnagee">github</Link>.
      <Link href="https://linkedin.com/in/isthisnagee">linkedin</Link>.
      <Link href="mailto:hello@isthisnagee.com">email</Link>.
    </p>
    <p>
      i'm currently at <Link href="https://joinhonor.com">honor</Link> helping
      build a better care experience.
    </p>
    <p>here are some songs i'm listening to:</p>
    <Music />
  </Layout>
);

export default IndexPage;
