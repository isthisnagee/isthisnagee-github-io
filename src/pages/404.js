import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';
import {Title, Link} from '../components/elements';

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <Title>
      NOT FOUND <br />
      {String.fromCodePoint(0x1f614)}
    </Title>
    <p>
      <Link to="/">click me to go back home {String.fromCodePoint(0x1f3e0)}</Link>
    </p>
  </Layout>
);

export default NotFoundPage;
