import PropTypes from 'prop-types';
import React from 'react';
import {SiteTitle} from './elements';

const Header = ({siteTitle}) => (
  <header>
    <div>
      <SiteTitle>{siteTitle}</SiteTitle>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
