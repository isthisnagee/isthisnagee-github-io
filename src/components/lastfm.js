import React, {useState, useEffect} from 'react';

import './lastfm.css';
import {cls} from './elements';

// ok like my api key is public. deal w it :)
const urlApiKey = 'api_key=99b3bff8e3eb1ef3d73429f2123f7e4d';
const urlFormat = 'format=json';
const urlUser = 'user=isthisnagee';
const urlMethod = 'method=user.getrecenttracks';
const urlApiBase = 'https://ws.audioscrobbler.com/2.0';
const recentTracksUrl = `${urlApiBase}/?${urlMethod}&${urlUser}&${urlApiKey}&${urlFormat}`;

function uniq(arr, key) {
  const setOfKeys = new Set();
  const uniqItems = [];

  arr.forEach(item => {
    let keyVal = null;
    if (typeof key === 'string') {
      keyVal = item[key];
    } else {
      keyVal = key(item);
    }
    if (setOfKeys.has(keyVal)) return;
    setOfKeys.add(keyVal);
    uniqItems.push(item);
  });

  return uniqItems;
}

function transformToTracks(data) {
  const {recenttracks: recentTracks} = data;

  const tracks = recentTracks.track.map(track => ({
    id: `${track.album['#text']}${track.name}`,
    album: track.album['#text'],
    url: track.url,
    song: track.name,
    artist: track.artist['#text'],
    nowPlaying: track['@attr'] && track['@attr'].nowplaying === 'true',
    imageUrl:
      track.image && track.image.length > 1 ? track.image[1]['#text'] : null,
  }));
  const [first, ...rest] = tracks;
  const uniqTracks = uniq([first, ...rest], 'id');
  return uniqTracks.slice(0, 16);
}

function Track({artist, song, url, imageUrl, nowPlaying}) {
  const description = `"${song}" by ${artist}`;
  return (
    <a href={url}>
      <img
        className={`music-block ${nowPlaying ? 'now-playing' : 'not-playing'}`}
        src={imageUrl}
        alt={description}
        title={description}></img>
    </a>
  );
}

Track.Loading = function Loading() {
  return <div className={cls('music-block', 'loading')} />;
};

function Music() {
  const [tracks, setTracks] = useState(null);

  function fetchSongs() {
    fetch(recentTracksUrl)
      .then(response => response.json())
      .then(data => setTracks(transformToTracks(data)));
  }

  useEffect(() => {
    fetchSongs();
    const interval = setInterval(fetchSongs, 10000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  if (tracks === null) {
    return <div className={cls('img-grid', 'loading')} />
  }

  return (
    <div className="img-grid">
      {tracks.map(({id, ...track}) => (
        <Track key={id} {...track} />
      ))}
    </div>
  );
}

export default Music;
