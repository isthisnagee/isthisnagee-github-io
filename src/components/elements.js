import React from 'react';
import {Link as GLink} from 'gatsby';

import './elements.css';

// first, filter out falsy stuff then join
const cls = (...classNames) => classNames.filter(c => c).join(' ');

const Title = ({className, children}) => (
  <h1 className={cls(className, 'title')}>{children}</h1>
);

const Link = ({href, to, className, children}) => {
  const props = {
    className: cls(className, 'a', 'link'),
    children,
  };
  if (to) {
    return <GLink to={to} {...props} />;
  }

  return (
    <a href={href} {...props}>
      {props.children}
    </a>
  );
};

const SiteLink = ({to, className, children}) => (
  <GLink to={to} className={cls(className, 'a')}>
    {children}
  </GLink>
);
const SiteTitle = ({children}) => (
  <Title className="site-title">
    <SiteLink to="/">{children}</SiteLink>
  </Title>
);

export {cls, Title, SiteTitle, Link};
